// var express = require('express');
// var app = express();
// app.get('/',function(req,res){
//     var sql = require("mssql");

//     var config={
//         user:'sa',
//         password:'123456',
//         server:'localhost',
//         database:'gts',
// 	port:1433
//     };

//     sql.connect(config,function(err){
//             if(err) console.log(err);
//             var request =new sql.Request();
//             request.query("select * from information_schema.tables",function(err,recordset){
//             if(err)console.log(err)
            
//             res.send(recordset);
//         });
//     });
// });


var express = require("express"); 
var app = express();

var mssql = require("mssql");
var dbConfig ={
    user: 'sa',
    password: '123456',
    server:'localhost',
    database: 'gts',
    port: 1433
};

var connection = mssql.connect(dbConfig,function(err){
    if(err)
       console.log(err); 
});  

app.get('/', function (req, res, next) {
    
    connection.query('select * from information_schema.tables', function (err, result) {
        if (err) 
            return next(err);
   
        var data = {};
        data["user"] = result.recordset;
        res.send(data);      
    }); 
}); 
var server = app.listen(5002, function () {
    console.log('Server is running..');
});  
